# Armor Converter

**Compatibility:** Classic Doom source ports supporting ACS and DECORATE.

This inventory item converts all player's armor into health. The value converted is weighted on the current armor's save percent.
